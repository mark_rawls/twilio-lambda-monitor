package main

import (
	"context"
	"fmt"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/sfreiberg/gotwilio"
	"net/http"
	"os"
	"strings"
)

func SendAlerts(url string, code int) {
	numbers := strings.Fields(os.Getenv("NUMBERS"))
	from := os.Getenv("TWILIO_FROM_NUMBER")
	accountSid := os.Getenv("TWILIO_ACCOUNT_SID")
	authToken := os.Getenv("TWILIO_AUTH_TOKEN")
	message := ""

	twilio := gotwilio.NewTwilioClient(accountSid, authToken)

	if code < 200 || code > 299 {
		message = fmt.Sprintf("%s is returning status code %d", url, code)
	} else {
		message = fmt.Sprintf("%s is unreachable!", url)
	}

	for _, to := range numbers {
		twilio.SendSMS(from, to, message, "", "")
		fmt.Printf("Sent for %s\n", url)
	}
}

func TestUrl(url string) (success bool, code int) {
	success = true
	code = 200

	resp, err := http.Get(url)
	if err != nil || (resp.StatusCode < 200 || resp.StatusCode > 299) {
		fmt.Println(err)
		success = false

		// Only set status code if we have it available
		if err == nil {
			code = resp.StatusCode
		}
	}

	return
}

func HandleRequest(ctx context.Context) {
	urls := strings.Fields(os.Getenv("URLS"))

	for _, url := range urls {
		success, code := TestUrl(url)
		if !success {
			SendAlerts(url, code)
		}
	}
}

func main() {
	lambda.Start(HandleRequest)
}
